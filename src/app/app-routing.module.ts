import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ResumeComponent } from './resume/resume.component';
import { SkillsComponent } from './skills/skills.component';

const routes: Routes = [
  {path: 'home' , component: HomeComponent},
  {path:'' , redirectTo:'home' , pathMatch:'full'},
  {path:'resume' , component: ResumeComponent},
  {path: 'skills' , component: SkillsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes , { scrollPositionRestoration: 'top' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
