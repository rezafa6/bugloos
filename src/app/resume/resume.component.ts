import { Component, OnInit } from '@angular/core';
import {ThemePalette} from '@angular/material/core';
import { FormGroup , FormArray , FormBuilder , FormControl , Validators } from '@angular/forms';
import { Router } from '@angular/router';

export interface Theme {
  color: ThemePalette;
}

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent implements OnInit {

  eduName = new FormControl(''  , [Validators.required]);
  uniName = new FormControl('', [Validators.required]);
  eduLvl = new FormControl('' ,[Validators.required]);
  jobTitle = new FormControl('', [Validators.required]);
  coName = new FormControl('', [Validators.required]);
  salary = new FormControl('');

  educate_jobs_form: FormGroup = this.fb.group({
  eduName: this.eduName,
  uniName: this.uniName,
  
  educates: this.fb.array([]) ,
  jobs: this.fb.array([])

  })

  // checkbox theme
  theme:Theme = {
    color: 'primary'
  }


  // get eduucates
  get educates() {
    return this.educate_jobs_form.controls['educates'] as FormArray
  }
// get jobs
  get jobs() {
    return this.educate_jobs_form.controls['jobs'] as FormArray
  }
// add jobs
jobForm: FormGroup | undefined
addJobs() {
   this.jobForm = this.fb.group({
    newJobTitle: new FormControl(''),
    newCoName: new FormControl('')
  })
  this.jobs.push(this.jobForm);
}

// add educates
addEducates() {
  const educateForm = this.fb.group({
    eduName:[''],
    uniName: [''],
    eduLvl: ['Diploma']
  })

  this.educates.push(educateForm)
}

// remove educates
removeEducate(educateIndex: number) {
this.educates.removeAt(educateIndex)
}

// remove jobs

removeJobs(jobIndex: number) {
this.jobs.removeAt(jobIndex)
}

// submit form

subEducates_jobs() {
  console.log(this.educate_jobs_form.value);
  this.router.navigateByUrl('skills')
}
  constructor(private fb: FormBuilder , private router: Router) { }

  
  setPriceToLocal: any;
  getSalary:any = this.salary.value;


  ngOnInit(): void {
   
  }
  spilt_salary() {
    
    // localStorage.clear()
      this.setPriceToLocal = localStorage.setItem('price' , this.getSalary)
  

   
    this.getSalary = localStorage.getItem('price')?.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "3,")
  
}
}
