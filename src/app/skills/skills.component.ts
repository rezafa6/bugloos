import { Component, OnInit } from '@angular/core';
import { GlobalVarService } from '../global-var.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  constructor(private glb: GlobalVarService) { }

  skillQuery: string | undefined;

  skillsList: any[] = ['html' , 'css' , 'scss' , 'javaScript' , 'php', 'java' , 'laravel',
'git' , 'python' , 'django' ,  'sql' , 'jquery' , 'bootstrap' , 'flexbox' , 'c#' , 'c++',
'reactjs' , 'flutter' , 'vuejs' , 'android' , 'swift' , 'typescript' , 'angular' , 
'material ui' , 'AI'];

showUserName:boolean = false;

name: string |undefined;
submitSkills() {
  this.showUserName = true;
}
  ngOnInit(): void {
    this.glb.getFullName.subscribe(
      name => this.name = name
    )
  }

  

}
