import { Component, OnInit } from '@angular/core';
import { FormBuilder , FormControl , FormGroup , Validators } from '@angular/forms';
import {ThemePalette} from '@angular/material/core';
import { Router } from '@angular/router';
import { GlobalVarService } from '../global-var.service';


export interface Theme {
  color: ThemePalette;
}
interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // pattern for gmail & phone number
  pattern: RegExp | undefined;
  // toggle add & remove new phone number
  addInputTgl: boolean = false
  // input controls & validation
  fullName = new FormControl('' , [Validators.required , Validators.minLength(5)]);
  phoneNum1 = new FormControl('' , [Validators.required]);
  phoneNum2 = new FormControl('')
  email = new FormControl('' , [Validators.required]);
  date = new FormControl('' , [Validators.required]);
  address = new FormControl('' , [Validators.required]);
  rule = new FormControl(false , [Validators.required]);

  constructor(private builder: FormBuilder , private router: Router ,
     private glb: GlobalVarService) { }

  // form group values
  IntroForm: FormGroup = this.builder.group({
    fullName: this.fullName,
    phoneNum1: this.phoneNum1,
    phoneNum2: this.phoneNum2,
    email: this.email,
    date: this.date,
    address: this.address,
    rule: this.rule,
  })



  //submit form
  SubIntro() {
    console.log(this.IntroForm.value);
    this.router.navigateByUrl('resume');
    this.glb.setName(this.fullName.value);
  }

  // checkbox theme
  theme:Theme = {
    color: 'primary'
  }

  // toggle new phone number function
  addNumberInput() {
    this.addInputTgl = !this.addInputTgl
  }


 



  ngOnInit(): void {
  }

}
