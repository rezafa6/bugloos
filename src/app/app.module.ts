import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ResumeComponent } from './resume/resume.component';
import { SkillsComponent } from './skills/skills.component';
import { SearchPipe } from './search.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ResumeComponent,
    SkillsComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatNativeDateModule ,
    MatInputModule,
    FormsModule,
    MatCheckboxModule,
    MatIconModule,
    MatSelectModule,
    MatFormFieldModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


// <form class="intro-form" >

// <input type="text" class="text-input" placeholder="جستجوی مهارت"
// [(ngModel)]="skillsQuery">


// <mat-checkbox *ngFor="let data of skillsList | searchSkills : skillsQuery"
// class="example-margin">{{data}}</mat-checkbox>
// </form>



// skillsList: any[] = ['html' , 'css' , 'scss' , 'javaScript' , 'php', 'java' , 'laravel',
// 'git' , 'python' , 'django' ,  'sql' , 'jquery' , 'bootstrap' , 'flexbox' , 'c#' , 'c++',
// 'reactjs' , 'flutter' , 'vuejs' , 'android' , 'swift' , 'typescript' , 'angular' , 
// 'material ui' , 'AI']