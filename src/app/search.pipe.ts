import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchSkills',
  pure: false
})

export class SearchPipe implements PipeTransform {

  transform(value: any, args?: any): any {
      if(!value)return null;
      if(!args)return value;
      return value.filter(function(data: any){
          return JSON.stringify(data).includes(args);
      });
  }

}