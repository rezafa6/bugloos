import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalVarService {

  private messageSource = new BehaviorSubject('');
  getFullName = this.messageSource.asObservable();

  constructor() { }

  setName(message: string) {
    this.messageSource.next(message)
  }
}
